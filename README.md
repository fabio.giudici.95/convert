# Convert Project

## Table of contents
1. [Download the converter](#download-the-converter)
2. [Test the application](#test-the-application)
3. [API Documentation](#api-documentation)
4. [Author](#author)

## Download the converter
Install docker if necessary.

```bash
docker pull fgiudici95/convert
docker run -p 49160:3000 -d fgiudici95/convert
```

## Test the application
```
curl --location --request GET 'http://localhost:49160/convert?amount=1&src_currency=EUR&dest_currency=USD&reference_date=2020-01-31'
```
The response must be:
```
{
    "amount": 1.1052,
    "currency": "USD"
}
```

## API Documentation
The [openapi.yaml](openapi.yaml) file contains the endpoint specification following the openapi format.

## Author
Fabio Giudici (fabio.giudici.95@gmail.com)
