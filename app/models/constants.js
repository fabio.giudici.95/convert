module.exports = {
  ECB_WS_ENTRYPOINT   : "https://sdw-wsrest.ecb.europa.eu/service/",
  ECB_RESOURCE        : "data/EXR/"
};
