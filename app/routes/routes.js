'use strict';
var path = require ( 'path' );

module.exports = function ( app )
{
  var appListConvert = require ( '../controllers/ctrlConvert' );

  // defining routes
  //
  app.route ( '/convert' ).get ( appListConvert.basicConvert );

  // other routes..
};
