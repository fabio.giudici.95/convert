'use strict';

var request = require ( 'request' );
var constants = require ( '../models/constants' );

exports.basicConvert = function ( req, res ) {
  var amount = req.query.amount;
  var src_currency = req.query.src_currency;
  var dest_currency = req.query.dest_currency;
  var reference_date = req.query.reference_date;
  //
  var url = constants.ECB_WS_ENTRYPOINT + constants.ECB_RESOURCE;
  var key = "";
  //
  // 0 : src_currency == EUR --> amount * rate
  // 1 : src_currency != EUR && dest_currency = EUR --> amount / rate
  // 2 : src_currency != EUR && dest_currency != EUR --> ( src_currency --> EUR --> dest_currency )
  //
  var convertBehaviour = 0;

  // Sanity check
  //
  if ( amount == null || amount == "" ) {
    res.status ( 400 );
    res.send ( { "error_msg" : "amount parameter not specified" } );
    return;
  }

  if ( src_currency == null || src_currency == "" ) {
    res.status ( 400 );
    res.send ( { "error_msg" : "src_currency parameter not specified" } );
    return;
  }

  if ( dest_currency == null || dest_currency == "" ) {
    res.status ( 400 );
    res.send ( { "error_msg" : "dest_currency parameter not specified" } );
    return;
  }

  if ( reference_date == null || reference_date == "" ) {
    res.status ( 400 );
    res.send ( { "error_msg" : "reference_date parameter not specified" } );
    return;
  }

  // Check parameters validity
  //
  if ( ( isNaN ( amount ) ) ) {
    res.status ( 400 );
    res.send ( { "error_msg" : "amount parameter must be a number" });
    return;
  }

  // Check date
  //
  if ( ! Date.parse ( reference_date ) ) {
    res.status ( 400 );
    res.send ( { "error_msg" : "reference_date parameter must be a date in format YYYY-MM-DD" });
    return;
  }

  // If the currencies are the same return immediately the amount
  //
  if ( src_currency == dest_currency ) {
    res.send ( { "amount" : Number(amount), "currency": dest_currency } );
    return;
  }

  // Select behaviour
  //
  if ( src_currency == "EUR" )
    convertBehaviour = 0;
  else if ( dest_currency == "EUR" )
    convertBehaviour = 1;
  else
    convertBehaviour = 2;

  // Compose key
  // key : the combination of dimensions allows statistical data to be uniquely identified
  // frequency.currency.EUR.SP00.A
  //
  if ( convertBehaviour == 0 )
    key = "D." + dest_currency + ".EUR.SP00.A";
  else
    key = "D." + src_currency + ".EUR.SP00.A";

  // Add query params
  //
  url += ( key + "?" );
  url += ( "startPeriod=" + reference_date + "&endPeriod=" + reference_date );

  url += "&detail=dataonly&dimensionAtObservation=AllDimensions";

  var ecb_request = {
    'method': 'GET',
    'url': url,
    'headers': {
      'Accept': 'application/json'
    }
  };

  request ( ecb_request, function ( error, response ) {
    if ( error ) {
      res.status ( 500 );
      res.send ( { "error_msg": error } );
      return;
    }

    if ( response.statusCode != 200 )
    {
      res.status ( response.statusCode );
      res.send ( { "error_msg": response.body } );
      return;
    }
    else {
      if ( response.body == "" ) {
        res.status ( 404 );
        res.send ( { "error_msg": "No result found." } );
        return;
      } else {
        var body = JSON.parse ( response.body );
        var rate = body.dataSets[0].observations["0:0:0:0:0:0"][0];
        //
        if ( convertBehaviour == 0 )
          amount = amount * rate;
        else if ( convertBehaviour == 1 )
          amount = amount / rate;
        //
        if ( convertBehaviour != 2 ) {
          res.send ( { "amount" : amount, "currency": dest_currency } );
          return;
        } else {
          amount = amount / rate;

          // Recompose key and URL
          //
          key = "D." + dest_currency + ".EUR.SP00.A";
          url = constants.ECB_WS_ENTRYPOINT + constants.ECB_RESOURCE + key + "?" +
                "startPeriod=" + reference_date + "&endPeriod=" + reference_date +
                "&detail=dataonly&dimensionAtObservation=AllDimensions";
          ecb_request.url = url;
          //
          request ( ecb_request, function ( error, response ) {
            if ( error ) {
              res.status ( 500 );
              res.send ( { "error_msg": error } );
              return;
            }

            if ( response.statusCode != 200 ) {
              res.status ( response.statusCode );
              res.send ( { "error_msg": response.body } );
              return;
            } else {
              var body = JSON.parse ( response.body );
              var rate = body.dataSets[0].observations["0:0:0:0:0:0"][0];
              amount = amount * rate;
              res.send ( { "amount" : amount, "currency": dest_currency } );
              return;
            }
          });
        }
      }
    }
  });
}
