const express = require ( 'express' );
const app = express();

app.listen ( 3000 );

//logger.info ( 'Server started on port ' + 3000 );

var routes = require ( './app/routes/routes' );
routes ( app );


module.exports = app;
