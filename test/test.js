const request = require('supertest');
const assert = require ( 'assert' );
const app = require ( "../server" );

describe('Missing parameters - respond with json error message ', function () {
    it('Without any parameters', function (done) {
        request(app)
            .get('/convert')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "amount parameter not specified"
            }, done);
    });

    it('Without amount', function (done) {
        request(app)
            .get('/convert?src_currency=EUR&dest_currency=EUR&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "amount parameter not specified"
            }, done);
    });

    it('Without src_currency', function (done) {
        request(app)
            .get('/convert?amount=1&dest_currency=EUR&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "src_currency parameter not specified"
            }, done);
    });

    it('Without dest_currency', function (done) {
        request(app)
            .get('/convert?amount=1&src_currency=EUR&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "dest_currency parameter not specified"
            }, done);
    });

    it('Without reference_date', function (done) {
        request(app)
            .get('/convert?amount=1&src_currency=EUR&dest_currency=EUR')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "reference_date parameter not specified"
            }, done);
    });
});

describe('Invalid parameter - respond with json error message ', function () {
    it('Amount is not a number', function (done) {
        request(app)
            .get('/convert?amount=abc&src_currency=EUR&dest_currency=EUR&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "amount parameter must be a number"
            }, done);
    });

    it('reference_date in a not valid format - 2020-13-01', function (done) {
        request(app)
            .get('/convert?amount=abc&src_currency=EUR&dest_currency=EUR&reference_date=2020-13-01')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "amount parameter must be a number"
            }, done);
    });

    it('reference_date in a not valid format - 2020-12-32', function (done) {
        request(app)
            .get('/convert?amount=abc&src_currency=EUR&dest_currency=EUR&reference_date=2020-12-32')
            .expect('Content-Type', /json/)
            .expect(400, {
              "error_msg": "amount parameter must be a number"
            }, done);
    });
});

describe('Same currency - respond with success response', function () {
    it('Request contains 1€, response must be contain 1€', function (done) {
        request(app)
            .get('/convert?amount=1&src_currency=EUR&dest_currency=EUR&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(200, {
              "amount": 1,
              "currency": "EUR"
            }, done);
    });
});

describe('Valid requests (2020-01-31) - respond with success response', function () {
    it('Request contains 1€, response must be contain 1.1052USD', function (done) {
        request(app)
            .get('/convert?amount=1&src_currency=EUR&dest_currency=USD&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(200, {
              "amount": 1.1052,
              "currency": "USD"
            }, done);
    });

    it('Request contains 1.1052USD, response must be contain 1€', function (done) {
        request(app)
            .get('/convert?amount=1.1052&src_currency=USD&dest_currency=EUR&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(200, {
              "amount": 1,
              "currency": "EUR"
            }, done);
    });

    it('Request contains 120.35JPY, response must be contain 1.1052USD', function (done) {
        this.timeout(5000);
        request(app)
            .get('/convert?amount=120.35&src_currency=JPY&dest_currency=USD&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(200, {
              "amount": 1.1052,
              "currency": "USD"
            }, done);
    });

    it('Request contains 1.1052USD, response must be contain 120.35JPY', function (done) {
        this.timeout(5000);
        request(app)
            .get('/convert?amount=1.1052&src_currency=USD&dest_currency=JPY&reference_date=2020-01-31')
            .expect('Content-Type', /json/)
            .expect(200, {
              "amount": 120.35,
              "currency": "JPY"
            }, done);
    });
});

describe ( 'Invalid requests - respond with error message', function () {
    it ( 'Request using an unexisting src_currency', function (done) {
      request(app)
          .get('/convert?amount=1&src_currency=AAA&dest_currency=EUR&reference_date=2020-01-31')
          .expect('Content-Type', /json/)
          .expect(404, {
            "error_msg": "No results found.",
          }, done);
    });

    it ( 'Request using an unexisting dest_currency', function (done) {
      request(app)
          .get('/convert?amount=1&src_currency=EUR&dest_currency=AAA&reference_date=2020-01-31')
          .expect('Content-Type', /json/)
          .expect(404, {
            "error_msg": "No results found.",
          }, done);
    });

    it ( 'Request using a future data', function (done) {
      request(app)
          .get('/convert?amount=1&src_currency=EUR&dest_currency=AAA&reference_date=2020-01-31')
          .expect('Content-Type', /json/)
          .expect(404, {
            "error_msg": "No results found.",
          }, done);
    });
});
